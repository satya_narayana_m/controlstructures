/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Apartment;

/**
 *This class provides methods for calculating rental prices for apartments.
 * @author Satyanarayana Madala
 */
public class Apartment {
    private double standardRentalAmount;
    private double seasonalRate;
    private double yearlyDiscount;
    /**
     * Constructor with arguments
     * @param standardRentalAmount Apartment Standard rental amount for a month
     * @param seasonalRate seasonal monthly rate for a month
     * @param yearlyDiscount yearly discount percentage for an apartment
     */
    public Apartment(double standardRentalAmount, double seasonalRate, double yearlyDiscount) {
        this.standardRentalAmount = standardRentalAmount;
        this.seasonalRate = seasonalRate;
        this.yearlyDiscount = yearlyDiscount;
    }
/**
 * Getter method to get Standard monthly rental amount
 * @return the standard monthly rental amount of an apartment
 */
    public double getStandardRentalAmount() {
        return standardRentalAmount;
    }
/**
 * Getter method to get seasonal rate of an apartment
 * @return the seasonal rate for a month 
 */
    public double getSeasonalRate() {
        return seasonalRate;
    }
/**
 * Getter method to get the yearly discount for an apartment
 * @return the yearly discount of an apartment
 */
    public double getYearlyDiscount() {
        return yearlyDiscount;
    }
/**
 * This method returns the rental amount for a specified month
 * @param month the specified month to return rent.
 * @return the monthly rental amount for a specified month
 */
    public double getMonthlyRental(int month){
        double monthlyRate;
        if(month==0 || month==11){
            monthlyRate= standardRentalAmount+(standardRentalAmount*seasonalRate);
        }else{
            monthlyRate=standardRentalAmount;
        }
        return monthlyRate;
    }
    /**
     * the method to return the total charged for a year-long lease.
     * @return the total charged  (as a double) for a year-long lease.
     */
    public double getYearlyRental(){
        return 12 * (1-yearlyDiscount) * standardRentalAmount;
    }
    /**
     * This method to get the total charged for a lease of several months
     * @param month starting month from which lease starts
     * @param numberOfMonths number of months in lease period
     * @return the total charged for a lease of several months
     */
    public double getMonthlyRental(int month, int numberOfMonths){
        
        int noOfYears=numberOfMonths/12;
        int noOfXtraMonths=numberOfMonths%12;
        double xtraMontlyRent=0.00;
        for (int i = 0; i < noOfXtraMonths; i++) {
            xtraMontlyRent+=getMonthlyRental(month%12);
            month++;
        }
        return noOfYears*getYearlyRental()+xtraMontlyRent;
    }
    /**
     * This method returns a string representation of the apartment object
     * @return the string representation of the apartment object
     */
    public String toString(){
        return "Appartment rental fee is $"+standardRentalAmount+"; the seasonal rate is "+seasonalRate+"; the yearly discount is "+yearlyDiscount;
    }
}
