/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Apartment;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *This class is to test the implementation of Apartment Class.
 * @author Satyanarayana Madala
 */
public class ApartmentDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {

         Scanner inAptData = new Scanner(new File("apartmentData.txt"));         
         while(inAptData.hasNext()){
             double standardRentalAmount = inAptData.nextDouble();
             double seasonalRate = inAptData.nextDouble();
             double yearlyDiscount = inAptData.nextDouble();
             Apartment apt1 = new Apartment(standardRentalAmount, seasonalRate, yearlyDiscount);
             System.out.println("NEW APARTMENT CREATED:");
             System.out.println(apt1);
             System.out.println("");
             
             System.out.println("TESTING GETTERS");
             System.out.println("Standard rental fee = "+apt1.getStandardRentalAmount());
             System.out.println("Seasonal rate = "+apt1.getSeasonalRate());
             System.out.println("Yearly Discount = "+apt1.getYearlyDiscount());
             System.out.println("");
             
             System.out.println("TESTING getMonthlyRental(int month)");
             for(int i=0; i<12; i++){
                 System.out.println("Rental fee for month "+i+ " is " +apt1.getMonthlyRental(i));
             }
             System.out.println("");
             
             System.out.println("TESTING getYearlyRental()");
             System.out.println("Yearly rental fee = "+apt1.getYearlyRental());
             System.out.println("");
             
             Scanner inMonthData = new Scanner(new File("monthData.txt"));
             while(inMonthData.hasNext()){
                 int month = inMonthData.nextInt();
                 int numberOfMonths = inMonthData.nextInt();
                 System.out.println("Rental fee for "+numberOfMonths+
                         " months, beginning with month "+month+" is " +apt1.getMonthlyRental(month, numberOfMonths));
             }
             System.out.println("");
             inMonthData.close();
         }
         inAptData.close();
    }
    
}
